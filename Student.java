public class Student {
    private int studentId;
    private String name;
    private String program;
    public int getStudentId() {
        return studentId;
    }
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getProgram() {
        return program;
    }
    public void setProgram(String program) {
        this.program = program;
    }
    public Student(int studentId, String name, String program) {
        this.studentId = studentId;
        this.name = name;
        this.program = program;
    }
    public String toString(){
        return ""+this.studentId+","+ this.name+ ","+this.program;
    }
}
