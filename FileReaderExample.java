
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileReaderExample{
    public static void main(String[] args) throws IOException {
        Path p = Paths.get("Example.txt");
        List<String> lines= Files.readAllLines(p);
        ArrayList<Student> sd = new ArrayList<Student>();
        //for each loop
        //putting them into an arrayList(of unknown size)
        for(String line:lines){
            String[] pieces =line.split(",");
            sd.add(new Student(Integer.parseInt(pieces[0]), pieces[1], pieces[2]));
        }
        //get the size for regular array later on
        int size= sd.size();
        Student[] collectionOfStudents= new Student[size];
        
        //putting them into a regular array
        for (int i=0; i<collectionOfStudents.length;i++){
            collectionOfStudents[i]=sd.get(i);
        }
        System.out.println("This is the arrayList");
        for(Student s:sd){
            System.out.println(s);
        }
        System.out.println("This is the regualr array");
        for(Student s:collectionOfStudents){
            System.out.println(s);
        }
}
}