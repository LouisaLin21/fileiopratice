import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.*;
public class FileWriterExample {
    public static void main (String[] args)throws IOException{
        List<Student> students = new ArrayList<Student>();
        students.add(new Student(10,"John","math"));
        students.add(new Student(11,"David","physics"));
        System.out.println(students.get(0).getStudentId());
        List<String> studentInString = new ArrayList <String>();
        for (int i=0; i<students.size(); i++){
            studentInString.add(students.get(i).toString());
        }
        Files.write(Paths.get("Example.txt"), studentInString);
    }
    
    
}
